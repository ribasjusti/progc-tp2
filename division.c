/*! \file division.c
 *  \author Justine Ribas <ribasjusti@cy-tech.fr>
 *  \version 0.1
 *  \date 15/11/2021
 *  \brief Programme pour l'exercice de la division de deux entiers
 */

 /* Inclusion des entetes de librairie */
#include <stdio.h>
#include <stdlib.h>

/*! \fn int main (int argc, char** argv)
 *  \author Justine Ribas <ribasjusti@cy-tech.fr>
 *  \version 0.1
 *  \date 15/11/2021
 *
 *  \brief Fonction principale
 * 
 *
 * \param argc : Nombre d'argument
 * \param argv : Tableau des arguments
 * \return 0   : le programme doit se terminer normalement
 *
 * \remarks 
 */

 int main (int argc, char** argv)
 {
    // Declaration des variables
    int int_n; //le numerateur
    int int_d; //le denominateur
    int int_retour1; //la valuer de retour du premier appel de scanf
    int int_retour2; //la valuer de retour du deuxieme appel de scanf

    // Initialisation des variables
    printf("Entrez le numerateur et le dénominateur : \n");
    int_retour1 = scanf("%d", &int_n);
    int_retour2 = scanf("%d", &int_d);

    // Verification des entrées
	if (int_retour1 == 0 || int_retour2 == 0) {
		printf ("Probleme de saisie\n");
		exit (-1);
	}

    // Division
    if(int_d != 0)
    {
        printf("Le quotient est : %d\n", int_n / int_d);
    }
    else
    {
        printf("Division par 0 impossible\n");
    }

    // Fin du programme, Il se termine normalement, et donc retourne 0
    return 0;
 }

 /* Réponse à la question :
    Lorsqu'on saisie un réel, le programme s'arrête.
    Pour véifier si le programme ne disfonctionne pas, on peut ajouter une condition dans le programme, afficher un message d'erreur et quitter le programme.
 */