/*! \file produit.c
 *  \author Justine Ribas <ribasjusti@cy-tech.fr>
 *  \version 0.1
 *  \date 15/11/2021
 *  \brief Programme pour l'exercice de compte à rebours
 */

 /* Inclusion des entetes de librairie */
#include <stdio.h>
#include <stdlib.h>


/*! \fn int main (int argc, char** argv)
 *  \author Justine Ribas <ribasjusti@cy-tech.fr>
 *  \version 0.1
 *  \date 15/11/2021
 *
 *  \brief Fonction principale
 * 
 *
 * \param argc : Nombre d'argument
 * \param argv : Tableau des arguments
 * \return 0   : le programme doit se terminer normalement
 *
 * \remarks 
 */

int main(int argc, char** argv)
{
    // Déclaration des variables
    int int_i; //iterrateur de boucle 
    int int_n; //nombre de départ du compte à rebours
    int int_retour; //valeur de retour de scanf

    // Saisie des variables
    do {
        printf("Saisissez un entier positif : ");
        int_retour = scanf("%d", &int_n);
    } while(int_n < 0);

    // Verification des entrées
	if (int_retour == 0) {
		printf ("Probleme de saisie\n");
		exit (-1);
	}

    // Affichage du compte à rebours
    for(int_i = int_n; int_i >= 0; int_i--){
        printf("%d\n", int_i);
    }

    // Fin du programme, Il se termine normalement, et donc retourne 0
    return 0;

}

/* Réponse qu9 :

    On peut également faire le compte à rebours grâce à une boucle while

    PROGRAMME Rebours
    n : entier
    lire(n)
    Tant que n > 0 :
        écrire(n)
        n <- n-1
    Fin Tant que
    FIN PROGRAMME

*/