/*! \file saisiePersonne.c
 *  \author Justine Ribas <ribasjusti@cy-tech.fr>
 *  \version 0.1
 *  \date 15/11/2021
 *  \brief Programme pour l'exercice de la saisie d'une personne
 */

 /* Inclusion des entetes de librairie */
#include <stdio.h>
#include <stdlib.h>

/*! \fn int main (int argc, char** argv)
 *  \author Justine Ribas <ribasjusti@cy-tech.fr>
 *  \version 0.1
 *  \date 15/11/2021
 *
 *  \brief Fonction principale
 * 
 *
 * \param argc : Nombre d'argument
 * \param argv : Tableau des arguments
 * \return 0   : le programme doit se terminer normalement
 *
 * \remarks 
 */
 
int main (int argc, char** argv){

    // Déclaration des variables
    char char_nom[29]; // Nom de la personne
    char char_prenom[29]; // Prenom de la personne
    int int_jour; // Jour de naissance de la personne
    char char_mois[29]; // Mois de naissance de la personne
    int int_annee; // Annee de naissance de la personne

    // Demande des informations a l'utilisateur

    printf("Veuillez saisir votre nom : ");
    scanf("%s", char_nom);

    printf("Veuillez saisir votre prenom : ");
    scanf("%s", char_prenom);

    do {
        printf("Veuillez saisir votre jour de naissance au bon format : ");
        scanf("%d", &int_jour);
    } while (int_jour < 1 || int_jour > 31);
    printf("Veuillez saisir votre mois de naissance : ");
    scanf("%s", char_mois);
    do {
        printf("Veuillez saisir votre annee de naissance au bon format : ");
        scanf("%d", &int_annee);
    } while (int_annee < 1900 || int_annee > 2020);

    // Affichage des informations saisies
    printf("\nVoici ce que vous avez entré :\nNom : %s\nPrénom : %s\nDate de naissance : %d %s %d\n", char_nom, char_prenom, int_jour, char_mois, int_annee);

    // Fin du programme, Il se termine normalement, et donc retourne 0
    return 0;
}