# TP - Entrées, Sorties

## Arborescence des fichiers
Chaque exercice correspond à un fichier différend
- L'exercice "Saisie d’un nombre" (qu1 et qu2) correspond au fichier "saisieNombre.c"   
- L'exercice "Saisie de deux entiers" (qu3) correspond au fichier "saisieDeuxEntiers.c"   
- L'exercice "Saisie d’une personne" (qu4) correspond au fichier "saisiePersonne.c"   
- L'exercice "Division d’entier" (qu5) correspond au fichier "division.c"   
- L'exercice "Produit de deux nombres" (qu6) correspond au fichier "produit.c"   
- L'exercice "Prédiction" (qu7) correspond au fichier "prediction.c"   
- L'exercice "Compte à rebours" (qu8 et qu9) correspond au fichier "compteRebours.c"

## Compilation 
Pour compiler chaque fichier, il faut saisir la commande suivante à la racine du dossier : gcc -Wall fichier.c -o nomProgramme

## Exécution :
De même, tapez la commande : ./nomProgrogramme

## Documentation doxygen :
Pour générer la documentation doxigen, tapez dans le terminal : doxygen Doxyfile   
Pour accéder à la documentation vous pouvez par exemple aller dans le dossier "html" et ouvrir le fichier "pages.html" dans un navigateur