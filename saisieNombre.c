/*! \file saisieNombre.c
 *  \author Justine Ribas <ribasjusti@cy-tech.fr>
 *  \version 0.1
 *  \date 15/11/2021
 *  \brief Programme pour l'exercice de la saisie d'un nombre
 */

 /* Inclusion des entetes de librairie */
#include <stdio.h>
#include <stdlib.h>

/*! \fn int main (int argc, char** argv)
 *  \author Justine Ribas <ribasjusti@cy-tech.fr>
 *  \version 0.1
 *  \date 15/11/2021
 *
 *  \brief Fonction principale
 * 
 *
 * \param argc : Nombre d'argument
 * \param argv : Tableau des arguments
 * \return 0   : le programme doit se terminer normalement
 *
 * \remarks 
 */

int main (int argc, char** argv)
{
   int int_nombre; // nombre que l'on souhaite saisir
   int int_retour; // retour de la fonction scanf

   // on demande à l'utilisateur de saisir un nombre
   printf("Saisir un nombre : ");
   int_retour = scanf("%d", &int_nombre);

   // on lui affiche la valeur de retour du scanf et le nombre saisi
   printf("scanf retourne %d et le nombre saisi est %d\n", int_retour, int_nombre);

   // Fin du programme, Il se termine normalement, et donc retourne 0
   return 0;
}

 /* Question 2 :

    lorsqu'on saisit un réel, le programme affiche sa partie entière et la valeur de retour de scanf vaut 1
    lorsqu'on saisit un caractère, le programme affiche un entier et la valeur de retour de scanf vaut 0
    idem lorsqu'on saisit une chaîne de caractères

*/


