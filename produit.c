/*! \file produit.c
 *  \author Justine Ribas <ribasjusti@cy-tech.fr>
 *  \version 0.1
 *  \date 15/11/2021
 *  \brief Programme pour l'exercice du produit de deux nombres
 */

 /* Inclusion des entetes de librairie */
#include <stdio.h>
#include <stdlib.h>

/*! \fn int main (int argc, char** argv)
 *  \author Justine Ribas <ribasjusti@cy-tech.fr>
 *  \version 0.1
 *  \date 15/11/2021
 *
 *  \brief Fonction principale
 * 
 *
 * \param argc : Nombre d'argument
 * \param argv : Tableau des arguments
 * \return 0   : le programme doit se terminer normalement
 *
 * \remarks 
 */

int main (int argc, char** argv)
 {
    // Declaration des variables
    double db_nb1; //le premier nombre
    double db_nb2; //le deuxieme nombre
    int int_retour1; //la valuer de retour du premier appel de scanf
    int int_retour2; //la valuer de retour du deuxieme appel de scanf

    // Initialisation des variables
    printf("Entrez les deux nombres : \n");
    int_retour1 = scanf("%lf", &db_nb1);
    int_retour2 = scanf("%lf", &db_nb2);

    // Verification des entrées
	if (int_retour1 == 0 || int_retour2 == 0) {
		printf ("Probleme de saisie\n");
		exit (-1);
	}

    // Affichage du resultat
    if (db_nb1 > 0 && db_nb2 > 0) {
        printf("Le produit est positif\n");
    } else {
        if (db_nb1 < 0 && db_nb2 < 0) {
            printf("Le produit est positif\n");
        } else {
            printf("Le produit est négatif\n");
        }
    }

    // Fin du programme, Il se termine normalement, et donc retourne 0
    return 0;
 }