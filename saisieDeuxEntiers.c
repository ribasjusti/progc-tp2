/*! \file saisieDeuxEntiers.c
 *  \author Justine Ribas <ribasjusti@cy-tech.fr>
 *  \version 0.1
 *  \date 15/11/2021
 *  \brief Programme pour l'exercice de la saisie de deux entiers
 */

 /* Inclusion des entetes de librairie */
#include <stdio.h>
#include <stdlib.h>

/*! \fn int main (int argc, char** argv)
 *  \author Justine Ribas <ribasjusti@cy-tech.fr>
 *  \version 0.1
 *  \date 15/11/2021
 *
 *  \brief Fonction principale
 * 
 *
 * \param argc : Nombre d'argument
 * \param argv : Tableau des arguments
 * \return 0   : le programme doit se terminer normalement
 *
 * \remarks 
 */

int main(int argc, char** argv)
{
    int int_nombre1; // premier nombre que l'on souhaite saisir
    int int_nombre2; // deuxième nombre que l'on souhaite saisir
    int int_retour1; // retour de la fonction scanf lors du premier appel
    int int_retour2; // retour de la fonction scanf lors du deuxième appel

    // on demande à l'utilisateur de saisir deux entiers
    printf("Saisissez deux entiers : \n");
    int_retour1 = scanf("%d", &int_nombre1);
    int_retour2 = scanf("%d", &int_nombre2);

    // Verification des entrées
	if (int_retour1 == 0 || int_retour2 == 0) {
		printf ("Probleme de saisie\n");
		exit (-1);
	}

    // on lui affiche la valeur de retour du scanf et le nombre saisi
    printf("scanf retourne %d pour nombre %d et elle retourne %d pour le nombre %d\n", int_retour1, int_nombre1, int_retour2, int_nombre2);

    // Fin du programme, Il se termine normalement, et donc retourne 0
    return 0;
}

/* Réponse aux questions :
    Lorsqu'on saisie un réel, le programme s'arrête.
    Pour véifier si le programme ne disfonctionne pas, on peut ajouter une condition dans le programme, afficher un message d'erreur et quitter le programme.
*/