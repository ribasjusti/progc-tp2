/*! \file produit.c
 *  \author Justine Ribas <ribasjusti@cy-tech.fr>
 *  \version 0.1
 *  \date 15/11/2021
 *  \brief Programme pour l'exercice de prédiction de l'heure
 */

 /* Inclusion des entetes de librairie */
#include <stdio.h>
#include <stdlib.h>


/*! \fn int main (int argc, char** argv)
 *  \author Justine Ribas <ribasjusti@cy-tech.fr>
 *  \version 0.1
 *  \date 15/11/2021
 *
 *  \brief Fonction principale
 * 
 *
 * \param argc : Nombre d'argument
 * \param argv : Tableau des arguments
 * \return 0   : le programme doit se terminer normalement
 *
 * \remarks 
 */

int main(int argc, char** argv)
{
    // Déclaration des variables
    int int_heure; // l'heure saisie par l'utilisateur
    int int_min; // les minutes saisies par l'utilisateur
    int int_retour1; // retour de scanf au premier appel
    int int_retour2; // retour de scanf au deuxième appel
    
    // Saisie de l'heure et vérification de la validité
    printf("Saisir l'heure : ");
    int_retour1 = scanf("%d", &int_heure);

    if (int_retour1 == 0) {
		printf ("Vous n'avez pas saisi un entier\n");
		exit (-1);
	}
    if (int_heure < 0 || int_heure > 23) {
        printf ("L'heure n'est pas au bon format\n");
        exit (-1);
    }


    // Saisie des minutes et vérification de la validité
    printf("Saisir les minutes : ");
    int_retour2 = scanf("%d", &int_min);
	
    if (int_retour2 == 0) {
		printf ("Vous n'avez pas saisi un entier\n");
		exit (-1);
	}
    if (int_min < 0 || int_min > 59) {
        printf ("Les minutes n'est pas au bon format\n");
        exit (-1);
    }
    

    // Calcul de l'heure
    if (int_heure == 23 && int_min == 59){
        int_heure = 0;
        int_min = 0;
    } else {
        if (int_min == 59){
            int_heure++;
            int_min = 0;
        } else {
        int_min++;
        }
    }
    

    // Affichage des variables
    printf("Dans une minute, il sera %d:%d\n", int_heure, int_min);

    // Fin du programme, Il se termine normalement, et donc retourne 0
    return 0;
}